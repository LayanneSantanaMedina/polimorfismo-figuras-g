using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo__Figuras_G._
{
    class FigurasGeometricas //Clase Base (padre)
    {
        public virtual void ladofiguras()
        {
            Console.WriteLine("Las figuras geometricas tienen lados, por ejemplo:");
        }
    }
    class Triangulo : FigurasGeometricas  //Clase derivada (hijo)
    {
        public override void ladofiguras()
        {
            Console.WriteLine("EL TRIANGULO tiene tres lados");

        }
    }
    class Rectangulo : FigurasGeometricas  //Clase derivada (hijo)
    {
        public override void ladofiguras()
        {
            Console.WriteLine("EL RECTANGULO tiene cuatro lados");

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            FigurasGeometricas myfigura = new FigurasGeometricas();
            FigurasGeometricas mytriangulo = new Triangulo();
            FigurasGeometricas myrectangulo = new Rectangulo();

            myfigura.ladofiguras();
            mytriangulo.ladofiguras();
            myrectangulo.ladofiguras();

            Console.WriteLine(myfigura);
            Console.Read();
        }
    }
}
